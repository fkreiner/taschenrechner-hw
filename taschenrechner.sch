EESchema Schematic File Version 4
LIBS:taschenrechner-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 850  800  850  550 
U 5DC3ED85
F0 "Sheet5DC3ED84" 50
F1 "keymatrix.sch" 50
$EndSheet
$Sheet
S 4200 2000 850  550 
U 5DD8FC99
F0 "Sheet5DD8FC98" 50
F1 "powersupply.sch" 50
$EndSheet
$Sheet
S 5150 2000 850  550 
U 5DD91A4A
F0 "Sheet5DD91A49" 50
F1 "uC.sch" 50
$EndSheet
$Sheet
S 6150 2000 850  550 
U 5DD91A65
F0 "Sheet5DD91A64" 50
F1 "display.sch" 50
$EndSheet
$EndSCHEMATC
